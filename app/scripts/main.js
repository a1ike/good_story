jQuery(function ($) {
  $(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate(
      {
        scrollTop: $($.attr(this, 'href')).offset().top,
      },
      500
    );
  });

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  $('.g-header__mob').on('click', function (e) {
    e.preventDefault();

    $(this).toggleClass('g-header__mob_active');
    $('.g-header').toggleClass('g-header_active');
    $('.g-header__nav').slideToggle('fast');
    $('.g-header__text').slideToggle('fast');
  });

  $('.g-reviews__button').on('click', function (e) {
    e.preventDefault();

    $('.g-partners').slideToggle('fast');
  });

  new Swiper('.g-gallery__cards', {
    spaceBetween: 20,
    navigation: {
      nextEl: '.g-gallery__next',
      prevEl: '.g-gallery__prev',
    },
    slidesPerView: 'auto',
    breakpoints: {
      760: {
        slidesPerView: 2,
        spaceBetween: 30,
        spaceBetween: 30,
        loop: true,
      },
      1200: {
        slidesPerView: 3,
        spaceBetween: 30,
        spaceBetween: 30,
        loop: true,
        pagination: {
          el: '.g-gallery .swiper-pagination',
        },
      },
    },
  });

  new Swiper('.g-video__cards', {
    loop: true,
    spaceBetween: 30,
    pagination: {
      el: '.g-video .swiper-pagination',
    },
    navigation: {
      nextEl: '.g-video__next',
      prevEl: '.g-video__prev',
    },
  });

  $(window).on('load resize', function () {
    if (window.matchMedia('(max-width: 1200px)').matches) {
      new SimpleBar(document.getElementById('simplebar'), { autoHide: false });
    }
  });

  $('#stars').barrating({
    theme: 'fontawesome-stars-o',
  });
});
